package com.epam.stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum Property implements AutoCloseable {
    INSTANCE;

    private Logger logger = LogManager.getLogger(Property.class);

    private final Properties properties;

    Property() {
        properties = new Properties();
        close();
    }

    public Properties getProperties(){
        return properties;
    }

    public String getProperty(String key){
        return properties.getProperty(key);
    }

    @Override
    public void close() {
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("config.properties")) {
            properties.load(input);
        } catch (FileNotFoundException e) {
            logger.error("Property file not found!");
        } catch (IOException e) {
            logger.error("Problems with loading properties.");
        }
    }
}

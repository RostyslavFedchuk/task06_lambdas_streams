package com.epam.stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamUsage {
    private static final int LIMIT;
    private static final int MIN_VALUE;
    private static final int MAX_VALUE;
    private static final int MIN_JUMP;
    private static final int MAX_JUMP;
    private static Logger logger = LogManager.getLogger(StreamUsage.class);
    private static Random random;

    static {
        LIMIT = Integer.valueOf(Property.INSTANCE.getProperty("LIMIT"));
        MIN_VALUE = Integer.valueOf(Property.INSTANCE.getProperty("MIN_VALUE"));
        MAX_VALUE = Integer.valueOf(Property.INSTANCE.getProperty("MAX_VALUE"));
        MIN_JUMP = Integer.valueOf(Property.INSTANCE.getProperty("MIN_JUMP"));
        MAX_JUMP = Integer.valueOf(Property.INSTANCE.getProperty("MAX_JUMP"));
        random = new Random();
    }

    public List<Integer> generateList() {
        List<Integer> list = Stream
                .generate(() -> random.nextInt(MAX_VALUE - MIN_VALUE) + MIN_VALUE)
                .limit(LIMIT)
                .collect(Collectors.toList());
        return list;
    }

    public List<Integer> generateListUsingIterate() {
        List<Integer> list = Stream
                .iterate(MIN_VALUE, n -> n + random.nextInt(MAX_JUMP - MIN_JUMP) + MIN_JUMP)
                .limit(LIMIT)
                .collect(Collectors.toList());
        return list;
    }

    public List<Integer> generateListUsingBuilder() {
        Stream.Builder<Integer> builder = Stream.builder();
        for (int i = MIN_VALUE; i < LIMIT; i++) {
            builder.add(random.nextInt(MAX_JUMP - MIN_JUMP) + MIN_JUMP);
        }
        return builder.build().collect(Collectors.toList());
    }

    public void getStatistics(List<Integer> list) {
        OptionalDouble averageValue = list.stream().mapToInt(v -> v).average();
        logger.info("Generated list:\n" + list + "\n");
        logger.info("Max value: " + list.stream().mapToInt(v -> v).max().getAsInt()
                + "\nMin value: " + list.stream().mapToInt(v -> v).min().getAsInt()
                + "\nAverage value: " + averageValue.getAsDouble()
                + "\nSum of values(using sum method): " + list.stream().mapToInt(Integer::intValue).sum()
                + "\nSum of values(using reduce method): " + list.stream().mapToInt(Integer::intValue).reduce((a, b) -> a + b).getAsInt());
        logger.info("\nNumbers that are greater than " + averageValue.getAsDouble() + ":\n");
        list.stream().filter(a -> a > averageValue.getAsDouble()).forEach(n -> logger.info(n + " "));
    }
}

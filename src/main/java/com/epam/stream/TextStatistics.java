package com.epam.stream;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TextStatistics {
    private static final Scanner SCANNER;
    private static Logger logger = LogManager.getLogger(TextStatistics.class);

    private String text;

    static {
        SCANNER = new Scanner(System.in);
    }

    {
        readText();
    }

    private void readText() {
        logger.info("\nEnter your text here: \n");
        StringBuilder text = new StringBuilder();
        String line;
        while (!(line = SCANNER.nextLine()).isEmpty()) {
            text.append(line + " ");
        }
        this.text = text.toString();
    }

    private List<String> getList(String text) {
        String[] textArray = text.split("[\\W\\s]+");
        List<String> list = Arrays.asList(textArray);
        return list.stream().sorted().collect(Collectors.toList());
    }

    private long countUnique(List<String> list) {
        return list.stream().distinct().count();
    }

    private Map<String, Long> getWordsCount(List<String> list) {
        return list.stream().collect(Collectors
                        .groupingByConcurrent(String::valueOf, Collectors.counting()));
    }

    private List<String> getCharacters(String text) {
        String[] textArray = text.split("");
        List<String> list = Arrays.asList(textArray);
        return list.stream().sorted().collect(Collectors.toList());
    }

    private Map<Character, Long> getSymbolCount() {
        return text.chars()
                .mapToObj(i -> (char) i)
                .filter(c -> c.toString().equals(c.toString().toLowerCase()))
                .collect(Collectors.groupingBy(Character::charValue, Collectors.counting()));
    }

    public void getStats() {
        List<String> words = getList(text);
        logger.info("Count of all unique words: " + countUnique(words)
                +"\nSorted list of all unique words:\n" + words.stream().distinct().collect(Collectors.toList())
                +"\nWords count of each word:\n" + getWordsCount(words)
                +"\nWords count of each symbol:\n" + getSymbolCount());
    }
}

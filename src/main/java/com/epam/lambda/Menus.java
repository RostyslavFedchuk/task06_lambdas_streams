package com.epam.lambda;

import java.util.LinkedHashMap;
import java.util.Map;

public class Menus {

    static private Map<String, String> menu;

    static{
        menu = new LinkedHashMap<>();
        menu.put("1", "Implement command as lambdas to delete all unnecessary symbols");
        menu.put("2", "Implement command as method reference to count length of your name");
        menu.put("3", "Implement command as anonymous class to uppercase first letter");
        menu.put("4", "Implement command as object of command class to lowercase all other letters");
        menu.put("Q", "Quit");
    }

    static public Map<String, String> getMenu() {
        return menu;
    }
}

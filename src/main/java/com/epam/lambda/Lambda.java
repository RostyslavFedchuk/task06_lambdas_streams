package com.epam.lambda;

import com.epam.lambda.interfaces.Command;
import com.epam.lambda.interfaces.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Lambda {
    private static Logger logger = LogManager.getLogger(Lambda.class);
    private static Scanner scanner;
    private Map<String, Command> menuMethods;

    static {
        scanner = new Scanner(System.in);
    }

    public Lambda() {
        fillMenuMethods();
    }

    @Override
    public String toString() {
        StringBuilder print = new StringBuilder("-----------Menu-----------\n");
        for (Map.Entry<String, String> entry : Menus.getMenu().entrySet()) {
            print.append(entry.getKey()).append(" - ").append(entry.getValue()).append("\n");
        }
        return print.toString();
    }

    private class CommandImpl implements Command {
        public void execute(String name) {
            String newName = name.substring(0, 1) + name.substring(1).toLowerCase();
            logger.info("Hello, " + newName + "\n");
        }
    }

    private void fillMenuMethods() {
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", (s) -> logger.info("Your name is "
                + s.replaceAll("[^a-zA-Z]+", "") + "\n"));
        menuMethods.put("2", this::printName);
        menuMethods.put("3", new Command() {
            @Override
            public void execute(String name) {
                String newName = name.substring(0, 1).toUpperCase() + name.substring(1);
                logger.info("Hello, " + newName + "\n");
            }
        });
        menuMethods.put("4", new CommandImpl());
        menuMethods.put("Q", this::quit);
    }

    private void printName(String name) {
        logger.info("Your name `"
                + name + "` has " + name.length() + " symbols\n");
    }

    private void quit(String name) {
        logger.info("Bye\n");
    }

    public void testFunctionInterface() {
        Random random = new Random();
        int first = random.nextInt();
        int second = random.nextInt();
        int third = random.nextInt();
        logger.info("Values: " + first + ", " + second + ", " + third + "\n");
        Function maxValue = (a, b, c) -> findMaxValue(a, b, c);
        Function averageValue = (a, b, c) -> ((a + b + c) / 3);
        logger.info("Max value: " + maxValue.carcass(first, second, third) + "\n");
        logger.info("Average value: " + averageValue.carcass(first, second, third) + "\n");
    }

    private static int findMaxValue(int... values) {
        int maxValue = Integer.MIN_VALUE;
        for (int value : values) {
            if (value > maxValue) {
                maxValue = value;
            }
        }
        return maxValue;
    }

    public void showMenu() {
        String option;
        logger.info("Enter your name: " + "\n");
        String name = scanner.next();
        do {
            logger.info(this + "\n");
            logger.info("Choose one option: " + "\n");
            option = scanner.next().toUpperCase();
            if (menuMethods.containsKey(option)) {
                menuMethods.get(option).execute(name);
            } else {
                logger.warn("Incorrect option input! Try again." + "\n");
            }
        } while (!option.equals("Q"));
    }
}

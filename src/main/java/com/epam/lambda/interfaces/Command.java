package com.epam.lambda.interfaces;

@FunctionalInterface
public interface Command {
    void execute(String name);
}

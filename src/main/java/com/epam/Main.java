package com.epam;

import com.epam.lambda.Lambda;
import com.epam.stream.StreamUsage;
import com.epam.stream.TextStatistics;

public class Main {
    public static void main(String[] args) {
        testHomeTask();
    }

    public static void testHomeTask(){
        Lambda lambda = new Lambda();
        lambda.testFunctionInterface();
        lambda.showMenu();
        StreamUsage stream = new StreamUsage();
        stream.getStatistics(stream.generateList());
        TextStatistics textStatistics = new TextStatistics();
        textStatistics.getStats();
    }
}